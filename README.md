This dashboard was originally created to connect to a classic Icinga server (very similar to Nagios) and was modified to use the Icinga2 API. To change the version you want to read from, modify the `$icingaversion` variable in `config.php`.

You will need to modify `config.php` to work with your installation of Icinga, specifically connection parameters and authentication variables.
