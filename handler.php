<?php

/*
 * Kansas State University
 * Computer Science
 *
 * CS System Monitoring
 *
 * handler.php
 *
 * Contains functions used for retrieving data from the Icinga server 
 *   and displaying results appropriately formatted.
 *
 * Created by Seth Galitzer <sgsax@ksu.edu>
 * Date Created: 10/22/2014
 * Updated: 10/17/2016
 */


/*********************/
/* Icinga1 functions */
/*********************/

function getJSON($myquery) {
/* handle connections to icinga1 classic web CGI */
    require "config.php";

    $url = "$icingaurl/$icingacgi?$myquery";
    $data = curl_init($url);
    curl_setopt_array($data, 
            array(
                CURLOPT_USERPWD => "$icingauser:$icingapass",
                CURLOPT_PORT => "$icingaport",
                CURLOPT_HTTPAUTH => 1,
                CURLAUTH_BASIC => 1,
                CURLOPT_RETURNTRANSFER => 1
                ));
    $raw = curl_exec($data);
    curl_close($data);
    return json_decode($raw, TRUE);
};

function getHostInfo($input) {
/* retrive information for hosts */
    $ret = array();
    foreach($input["status"]["host_status"] as $host) {
        array_push($ret,
                array(
                    "host_name" => $host["host_name"],
                    "status" => $host["status"],
                    "status_information" => $host["status_information"]
                    )
                );
    };
    return $ret;
};

function getServiceInfo($input) {
/* retrieve information for services */
    $ret = array();
    foreach($input["status"]["service_status"] as $host) {
        array_push($ret,
                array(
                    "host_name" => $host["host_name"],
                    "service" => $host["service_display_name"],
                    "last_check" => $host["last_check"],
                    "status" => $host["status"],
                    "acknowledged" => $host["has_been_acknowledged"],
                    "status_information" => $host["status_information"]
                    )
                );
    };
    return $ret;
};

function getHostgroupInfo($input) {
/* retrieve information for hostgroups */
    $ret = array();
    foreach($input["status"]["hostgroup_summary"] as $hostgrp) {
        array_push($ret,
            array(
                "hostgroup_name" => $hostgrp["hostgroup_name"],
                "hosts_up" => $hostgrp["hosts_up"],
                "hosts_down" => $hostgrp["hosts_down"],
                "services_ok" => $hostgrp["services_ok"],
                "services_warning" => $hostgrp["services_warning_unacknowledged"],
                "services_critical" => $hostgrp["services_critical_unacknowledged"]
                )
            );
    };
    return $ret;
}

function getHostDetails($input) {
/* retrive service information for a specific host */
    $ret = array();
    foreach($input["status"]["service_status"] as $host) {
        array_push($ret,
                array(
                    "service_name" => $host["service_display_name"],
                    "status" => $host["status"],
                    "last_check" => $host["last_check"],
                    "duration" => $host["duration"],
                    "acknowledged" => $host["has_been_acknowledged"],
                    "info" => $host["status_information"]
                    )
                );
    };
    return $ret;
};

function getHostgroupDetails($input) {
/* retrive host and service information for a specific hostgroup */
    $ret = array();
    foreach($input["status"]["hostgroup_overview"][0]["members"] as $host) {
        array_push($ret, array(
            $host["host_name"] => array(
                "host_status" => $host["host_status"],
                "services_ok" => $host["services_status_ok"],
                "services_warning" => $host["services_status_warning"],
                "services_critical" => $host["services_status_critical"],
                "services_unknown" => $host["services_status_unknown"]
                )
            )
        );
    };
    return $ret;
};

/*********************/
/* Icinga2 functions */
/*********************/

function getJSON2($query, $objects) {
/* handle connections to icinga2 REST API */
    require "config.php";

    $ret = array();

    $headers = array(
        'Accept: application/json',
        'X-HTTP-Method-Override: GET'
    );
    $data = curl_init();
    curl_setopt_array($data,
            array(
                CURLOPT_URL => "$icinga2url/$icinga2apipath/$objects",
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_USERPWD => "$icingauser:$icingapass",
                CURLOPT_PORT => "$icinga2port",
                CURLOPT_CAINFO => $icinga2ca,
                CURLOPT_SSL_VERIFYPEER => 1,
                CURLOPT_HTTPAUTH => 1,
                CURLAUTH_BASIC => 1,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST => count($query),
                CURLOPT_POSTFIELDS => json_encode($query)
                ));
    $raw = curl_exec($data);
    $code = curl_getinfo($data, CURLINFO_HTTP_CODE);
    if ($raw === false) {
        print "Error ($code): " . curl_error($data) . "($raw)\n";
    }
    curl_close($data);
    if ($code == 200) {
        $ret = json_decode($raw, TRUE);
    }

    return $ret;
};

function getHostInfo2($input) {
/* retrive information for hosts */
    $ret = array();
    if (count($input["hosts"]) > 0) {
        foreach($input["hosts"]["results"] as $host) {
            array_push($ret,
                    array(
                        "host_name" => $host["name"],
                        "status" => getHostStatus2($host["attrs"]["state"]),
                        "status_information" => $host["attrs"]["last_check_result"]["output"]
                        )
                    );
        };
    };
    return $ret;
};

function getServiceInfo2($input) {
/* retrieve information for services */
    $ret = array();

    if (count($input["services"]) > 0 ) {
        foreach($input["services"]["results"] as $service) {
            array_push($ret,
                    array(
                        "host_name" => $service["joins"]["host"]["name"],
                        "service" => $service["attrs"]["display_name"],
                        "last_check" => date(DATE_RSS,$service["attrs"]["last_check"]),
                        "status" => getServiceStatus2($service["attrs"]["state"]),
                        "acknowledged" => $service["attrs"]["acknowledgement"],
                        "status_information" => $service["attrs"]["last_check_result"]["output"]
                        )
                    );
        };
    };

    return $ret;
};

function getHostgroupInfo2($input) {
/* retrieve information for hostgroups */
    $hostgroups = array();

    foreach($input["results"] as $result) {
        foreach($result["joins"]["host"]["groups"] as $group) {
            if ((!preg_match('/.*debian.*/',$group))&&
                (!preg_match('/.*ubuntu.*/',$group))&&
                (!preg_match('/.*desktops.*/',$group))) {
                if (!array_key_exists($group, $hostgroups)){
                    $hostgroups[$group] = array(
                        "hostgroup_name" => $group,
                        "hosts_up" => 0,
                        "hosts_down" => 0,
                        "host_list" => array(),
                        "services_ok" => 0,
                        "services_warning" => 0,
                        "services_critical" => 0
                        );
                };
                if (array_search($result["joins"]["host"]["name"], $hostgroups[$group]["host_list"]) === FALSE) {
                    if ($result["joins"]["host"]["state"]==0) {
                        $hostgroups[$group]["hosts_up"]++;
                    } else {
                        $hostgroups[$group]["hosts_down"]++;
                    };
                    array_push($hostgroups[$group]["host_list"],$result["joins"]["host"]["name"]);
                };
                switch($result["attrs"]["state"]) {
                    case 0:
                        $hostgroups[$group]["services_ok"]++;
                        break;
                    case 1:
                        $hostgroups[$group]["services_warning"]++;
                        break;
                    case 2:
                        $hostgroups[$group]["services_critical"]++;
                        break;
                }
            }
        }
    }
    
    return $hostgroups;
}

function getHostDetails2($input) {
/* retrive service information for a specific host */
    $ret = array();
    foreach($input["results"] as $service) {
        array_push($ret,
                array(
                    "service_name" => $service["attrs"]["display_name"],
                    "status" => getServiceStatus2($service["attrs"]["state"]),
                    "last_check" => date(DATE_RSS, $service["attrs"]["last_check"]),
                    "duration" => dateDifference($service["attrs"]["last_check"],$service["attrs"]["last_hard_state_change"]),
                    "acknowledged" => $service["attrs"]["acknowledgement"],
                    "info" => $service["attrs"]["last_check_result"]["output"]
                    )
                );
    };
    return $ret;
};

function getHostgroupDetails2($input) {
/* retrive host and service information for a specific hostgroup */
    $ret = array();

    foreach($input["results"] as $service) {
        $hostname = $service["joins"]["host"]["name"];
        if (!array_key_exists($hostname, $ret)) {
            $ret[$hostname] = array(
                    'host_status' => getHostStatus2($service["joins"]["host"]["state"]),
                    'services_ok' => 0,
                    'services_warning' => 0,
                    'services_critical' => 0,
                    'services_unknown' => 0
                    );
        };

        switch($service["attrs"]["state"]) {
            case 0:
                $ret[$hostname]["services_ok"]++;
                break;
            case 1:
                $ret[$hostname]["services_warning"]++;
                break;
            case 2:
                $ret[$hostname]["services_critical"]++;
                break;
            default:
                $ret[$hostname]["services_unknown"]++;
                break;
        }
    };
    return $ret;
};

/*******************************/
/* output formatting functions */
/*******************************/

function printHostInfo($info, $icingaversion) {
/* diplay host information */
    if ($icingaversion == 1) {
        $hosts = getHostInfo($info);
    } else {
        $hosts = getHostInfo2($info);
    }
    $i=1;

    print "<div class=\"row\">\n";
    foreach($hosts as $host){
        print "    <div class=\"col-lg-2\">\n";
        print "        <div class=\"alert alert-danger\">\n";
        print "            <a href=\"host.php?host=$host[host_name]\" class=\"alert-link\"><strong>$host[host_name]</strong> - ($host[status])</a><br>\n";
        print "            $host[status_information]\n";
        print "        </div><!-- alert -->\n";
        print "    </div><!-- col -->\n";
        if(($i%6) == 0) {
            print "</div><!-- row -->\n";
            print "<div class=\"clearfix visible-lg-block\"></div>\n";
            print "<div class=\"row\">\n";
            $i=1;
        } else {
            $i+=1;
        }
    };
    print "</div><!-- row -->\n";
};

function printServiceInfo($info, $icingaversion) {
/* display service information */
    if ($icingaversion == 1) {
        $services = getServiceInfo($info);
    } else {
        $services = getServiceInfo2($info);
    }
    
    $i=1;

    print "<div class=\"row\">\n";
    foreach($services as $service) {
        if(strpos($service["status_information"], "CHECK_NRPE") !== 0) {
            print "    <div class=\"col-lg-2\">\n";
            if(strcmp($service["status"],"WARNING") == 0) {
                print "        <div class=\"alert alert-warning\">\n";
            } elseif (strcmp($service["status"],"CRITICAL") == 0) {
                print "        <div class=\"alert alert-danger\">\n";
            } else {
                print "        <div class=\"alert alert-info\">\n";
            };
            print "            <a href=\"host.php?host=$service[host_name]\" class=\"alert-link\"><strong>$service[host_name]</strong></a><br>\n";
            print "            $service[service] - $service[status]<br>\n";
            print "            $service[status_information]\n";
            print "        </div><!-- alert -->\n";
            print "    </div><!-- col -->\n";
            if(($i%6) == 0) {
                print "</div><!-- row -->\n";
                print "<div class=\"clearfix visible-lg-block\"></div>\n";
                print "<div class=\"row\">\n";
                $i=1;
            } else {
                $i+=1;
            };
        };
    };
    print "</div><!-- row -->\n";
};

function printHostgroupInfo($hostgrps) {
/* display hostgroup information */
    $i=1;

    print "<div class=\"row\">\n";
    foreach($hostgrps as $hostgrp) {
        $cnt = count($hostgrp["host_list"]); //$hostgrp["hosts_up"] + $hostgrp["hosts_down"];
        print "    <div class=\"col-lg-2\">\n";
        if(($hostgrp["hosts_down"] > 0) ||
           ($hostgrp["services_critical"] > 0)) {
            print "        <div class=\"alert alert-danger\">\n";
        } elseif ($hostgrp["services_warning"] > 0) {
            print "        <div class=\"alert alert-warning\">\n";
        } else {
            print "        <div class=\"alert alert-success\">\n";
        };
        print "            <a href=\"hostgroup.php?hostgroup=$hostgrp[hostgroup_name]\" class=\"alert-link\"><strong>$hostgrp[hostgroup_name]</strong> ($cnt hosts)</a><br>\n";
        print "            <div class=\"row\">\n";
        print "                <div class=\"col-lg-6\">\n";
        if($hostgrp["hosts_down"] > 0) {
            print "                    <div class=\"alert alert-danger\">\n";
        } else {
            print "                    <div class=\"alert alert-success\">\n";
        }
        print "                        <strong>Hosts</strong><br>\n";
        print "                        UP: $hostgrp[hosts_up]<br>\n";
        print "                        DOWN: $hostgrp[hosts_down]<br>\n";
        print "                    </div><!-- alert -->\n";
        print "                </div><!-- col -->\n";
        print "                <div class=\"col-lg-6\">\n";
        if($hostgrp["services_critical"] > 0) {
            print "                    <div class=\"alert alert-danger\">\n";
        } elseif ($hostgrp["services_warning"] > 0) {
            print "                    <div class=\"alert alert-warning\">\n";
        } else {
            print "                    <div class=\"alert alert-success\">\n";
        }
        print "                        <strong>Services</strong><br>\n";
        print "                        OK: $hostgrp[services_ok]<br>\n";
        print "                        WARN: $hostgrp[services_warning]<br>\n";
        print "                        CRIT: $hostgrp[services_critical]<br>\n";
        print "                    </div><!-- alert -->\n";
        print "                </div><!-- col -->\n";
        print "            </div><!-- row -->\n";
        print "        </div><!-- alert -->\n";
        print "    </div><!-- col -->\n";
        if(($i%6) == 0) {
            print "</div><!-- row -->\n";
            print "<div class=\"clearfix visible-lg-block\"></div>\n";
            print "<div class=\"row\">\n";
            $i=1;
        } else {
            $i+=1;
        };
    };
    print "</div><!-- row -->\n";
};

function printHostgroupDetails($info, $icingaversion) {
/* display information for a specific hostgroup */
    if ($icingaversion == 1) {
        $hosts = getHostgroupDetails($info);
    } else {
        $hosts = getHostgroupDetails2($info);
    }
    $i=1;

    print "<div class=\"row\">\n";
    foreach($hosts as $hostname => $host) {
        print "    <div class=\"col-lg-2\">\n";
        if($host["host_status"] == "DOWN") {
            print "        <div class=\"alert alert-danger\">\n";
        } else {
            print "        <div class=\"alert alert-success\">\n";
        };
        print "            <a href=\"host.php?host=$hostname\" class=\"alert-link\"><strong>$hostname - $host[host_status]</strong></a><br>\n";
        print "            <div class=\"row\">\n";
        print "                <div class=\"col-lg-12\">\n";
        if($host["services_critical"] > 0) {
            print "                    <div class=\"alert alert-danger\">\n";
        } elseif ($host["services_warning"] > 0) {
            print "                    <div class=\"alert alert-warning\">\n";
        } elseif ($host["services_unknown"] > 0) {
            print "                    <div class=\"alert alert-info\">\n";
        } else {
            print "                    <div class=\"alert alert-success\">\n";
        }
        print "                        <strong>Services</strong><br>\n";
        print "                        <table class=\"table-condensed\">\n";
        print "                            <tr>\n";
        print "                                <td>OK: $host[services_ok]</td>\n";
        print "                                <td>CRITICAL: $host[services_critical]</td>\n";
        print "                            </tr>\n";
        print "                            <tr>\n";
        print "                                <td>WARNING: $host[services_warning]</td>\n";
        print "                                <td>UNKOWN: $host[services_unknown]</td>\n";
        print "                            </tr>\n";
        print "                        </table>\n";
        print "                    </div><!-- alert -->\n";
        print "                </div><!-- col -->\n";
        print "            </div><!-- row -->\n";
        print "        </div><!-- alert -->\n";
        print "    </div><!-- col -->\n";
        if(($i%6) == 0) {
            print "</div><!-- row -->\n";
            print "<div class=\"clearfix visible-lg-block\"></div>\n";
            print "<div class=\"row\">\n";
            $i=1;
        } else {
            $i+=1;
        };
    };
    print "</div><!-- row -->\n";
};

function printHostDetails($info, $icingaversion) {
/* display information for a specific host */
    if ($icingaversion == 1) {
        $svcs = getHostDetails($info);
    } else {
        $svcs = getHostDetails2($info);
    }

    print "<table class=\"table table-striped\">\n";
    print "    <tr><th>Service</th><th>Status</th><th>Ack</th><th>Last Check</th><th>Duration</th><th>Details</th></tr>\n";
    foreach($svcs as $svc) {
        print "    <tr";
        if($svc["status"] == "CRITICAL") {
            print " class=\"danger\"";
        } elseif($svc["status"] == "WARNING") {
            print " class=\"warning\"";
        } elseif($svc["status"] == "UNKNOWN") {
            print " class=\"info\"";
        }
        print ">\n";
        print "        <td>$svc[service_name]</td>\n";
        print "        <td>$svc[status]</td>\n";
        print "        <td>";
        if($svc["acknowledged"]) {
            print "YES";
        } elseif (!($svc["acknowledged"]) && ($svc["status"] != "OK")) {
            print "NO";
        }
        print "</td>\n";
        print "        <td>$svc[last_check]</td>\n";
        print "        <td>$svc[duration]</td>\n";
        print "        <td>$svc[info]</td>\n";
        print "    </tr>\n";
    }
    print "</table>\n";
}

function printServiceDetails($info, $icingaversion) {
/* diplay information for a specific host's services */
    if ($icingaversion == 1) {
        $svcs = getServiceInfo($info);
    } else {
        $svcs = getServiceInfo2($info);
    }

    print "<table class=\"table table-striped\">\n";
    print "    <tr><th>Host</th><th>Service</th><th>Status</th><th>Ack</th><th>Last Check</th><th>Details</th></tr>\n";
    foreach($svcs as $svc) {
        print "    <tr";
        if($svc["status"] == "CRITICAL") {
            print " class=\"danger\"";
        } elseif($svc["status"] == "WARNING") {
            print " class=\"warning\"";
        } elseif($svc["status"] == "UNKNOWN") {
            print " class=\"info\"";
        }
        print ">\n";
        print "        <td>$svc[host_name]</td>\n";
        print "        <td>$svc[service]</td>\n";
        print "        <td>$svc[status]</td>\n";
        print "        <td>";
        if($svc["acknowledged"]) {
            print "YES";
        } elseif (!($svc["acknowledged"]) && ($svc["status"] != "OK")) {
            print "NO";
        }
        print "</td>\n";
        print "        <td>$svc[last_check]</td>\n";
        print "        <td>$svc[status_information]</td>\n";
        print "    </tr>\n";
    }
    print "</table>\n";

}

/********************/
/* helper functions */
/********************/

function strmatch($needle, $haystack) {
/* implement a substring search function for php, returns true if found or */
/*   false if not found */
    if (strpos($haystack, $needle) !== false) {
        $ret = true;
    } else {
        $ret = false;
    }

    return $ret;
}

function dateDifference($date_1, $date_2) {
/* calculate the difference between two dates and return in day:hr:min:sec */
    $datetime1 = date_create(date(DATE_RSS,$date_1));
    $datetime2 = date_create(date(DATE_RSS,$date_2));
   
    $interval = date_diff($datetime1, $datetime2);
   
    return $interval->format('%dd %hh %im %ss');
}

function countHostsInGroup($raw, $icingaversion) {
/* count the hosts in a hostgroup */
    if ($icingaversion == 1) {
        return count(getHostgroupDetails($raw));
    } else {
        return count(getHostgroupDetails2($raw));
    }
}

function getHostStatus2($status) {
/* translate icinga2 host status codes into icinga1-style status */
    switch ($status) {
    case  0: 
        $ret = "UP";
        break;
    case 1:
        $ret = "DOWN";
        break;
    default:
        $ret = "UNKNOWN";
        break;
    }

    return $ret;
}

function getServiceStatus2($status) {
/* translate icinga2 service status codes into icinga1-style status */
    switch ($status) {
    case  0: 
        $ret = "OK";
        break;
    case 1:
        $ret = "WARNING";
        break;
    case 2:
        $ret = "CRITICAL";
        break;
    default:
        $ret = "UNKNOWN";
        break;
    }

    return $ret;

}

/***********************************/
/* Icinga2 dynamic query functions */
/***********************************/

/* These are used when static queries are not possible, ie when you want to */
/*   filter on a specific host or hostgroup which is passed in as a parameter */
function query2hostgroup($hostgroup) {
    $ret = array(
        'attrs' => array('name', 'display_name', 'state'),
        'joins' => array('host.name', 'host.state'),
        'filter' => "\"$hostgroup\" in host.groups"
    );
    return $ret;
};

function query2host($host) {
    $ret = array(
        'attrs' => array('name', 'state', 'display_name', 'last_check', 'acknowledgement', 'last_hard_state_change', 'last_check_result'),
        'joins' => array('host.name', 'host,state'),
        'filter' => "match(\"$host\", host.name)"
    );
    return $ret;
};

function query2servicedetail($hostgroup) {
    $ret = array(
        'attrs' => array('name', 'state', 'display_name', 'last_check', 'acknowledgement', 'last_hard_state_change', 'last_check_result'),
        'joins' => array('host.name', 'host,state'),
        'filter' => "\"$hostgroup\" in host.groups&&service.state!=ServiceOK&&service.downtime_depth==0&&service.acknowledgement==0&&host.downtime_depth==0&&host.acknowledgement==0&&!match(\"*ping*\",service.name)"
    );
    return $ret;
};

?>
