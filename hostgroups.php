<?php
/*
 * Kansas State University
 * Computer Science
 *
 * CS System Monitoring
 *
 * hostgroups.php
 *
 * prepare and display information for all hostgroups
 *
 * Created by Seth Galitzer <sgsax@ksu.edu>
 * Date Created: 10/22/2014
 * Updated: 10/17/2016
 */

//    session_start();

    require_once "config.php";
    require_once "handler.php";

//    // initialize session token
//    $_SESSION["token"] = md5(mt_rand());

    if ($icingaversion == 1) {
        $json_data = getJSON($queryhostgroups);
        $hostgroups = getHostgroupInfo($json_data);
    } else {
        $json_data = getJSON2($query2hostgroups, "services");
        $hostgroups = getHostgroupInfo2($json_data);
    }

?>

<!DOCTYPE html>
<html>
<head>
    <title>Systems Status - All Hostgroups</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/icingahud.css" />
</head>
<body>
    <?php include "nav.php"; ?>

    <div class="container">
        <div class="row">
            <div class="span12">
                <p class="lead">Hostgroups Overview</p>
                <h2>Hostgroups (<?php print count($hostgroups); ?> groups)</h2>
                <?php printHostgroupInfo($hostgroups); ?>
            </div><!-- /.span12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->

</body>
</html>
