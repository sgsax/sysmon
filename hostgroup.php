<?php
/*
 * Kansas State University
 * Computer Science
 *
 * CS System Monitoring
 *
 * hostgroup.php
 *
 * prepare and display infomration for a specific hostgroup
 *
 * Created by Seth Galitzer <sgsax@ksu.edu>
 * Date Created: 10/22/2014
 * Updated: 10/17/2016
 */

//    session_start();

    require_once "config.php";
    require_once "handler.php";

//    // initialize session token
//    $_SESSION["token"] = md5(mt_rand());

    $hostgroup="";

    if (!empty($_GET)) {
        $hostgroup=htmlspecialchars($_GET["hostgroup"]);
    }

    if($hostgroup != "") {
        if ($icingaversion == 1) {
            $json_data = getJSON($queryhostgroup . $hostgroup);
            $hostcount = countHostsInGroup($json_data, $icingaversion);
        } else {
            $json_data = getJSON2(query2hostgroup($hostgroup), "services");
            $hostcount = countHostsInGroup($json_data, $icingaversion);
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>Systems Status - <?php echo $hostgroup; ?></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/icingahud.css" />
</head>
<body>
    <?php include "nav.php"; ?>

    <div class="container">
        <div class="row">
            <div class="span12">
                <p class="lead">Hostgroup Overview for <?php print $hostgroup; ?></p>
                <h2><?php print $hostgroup; ?> (<?php print $hostcount; ?> hosts)</h2>
                <?php
                    if($hostgroup != "") {
                        printHostgroupDetails($json_data, $icingaversion);
                    } else {
                        print "No hostgroup selected<br>\n";
                    }
                ?>
            </div><!-- /.span12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->

</body>
</html>
