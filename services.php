<?php
/*
 * Kansas State University
 * Computer Science
 *
 * CS System Monitoring
 *
 * services.php
 *
 * prepare and display information for services with problems
 *
 * Created by Seth Galitzer <sgsax@ksu.edu>
 * Date Created: 10/22/2014
 * Updated: 10/17/2016
 */

//    session_start();

    require_once "config.php";
    require_once "handler.php";

//    // initialize session token
//    $_SESSION["token"] = md5(mt_rand());

    if ($icingaversion == 1) {
        $json_data = getJSON($queryall);
    } else {
        $json_services = getJSON2($query2services, "services");
        $json_data = array('services' => $json_services);
    }

?>

<!DOCTYPE html>
<html>
<head>
    <title>Systems Status - All Service Problems</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/icingahud.css" />
</head>
<body>
    <?php include "nav.php"; ?>

    <div class="container">
        <div class="row">
            <div class="span12">
                <p class="lead"> Service Problems Overview</p>
                <h2>Services</h2>
                <?php printServiceInfo($json_data, $icingaversion); ?>
            </div><!-- /.span12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->

</body>
</html>
