<?php

/*
 * Kansas State University
 * Computer Science
 *
 * CS System Monitoring
 *
 * config.php
 *
 * contains configuration data used by application
 *
 * Created by Seth Galitzer <sgsax@ksu.edu>
 * Date Created: 10/22/2014
 * Updated: 10/17/2016
 */

// indicate the version of Icinga you are connectiong to: [1|2]
$icingaversion=2;

// Icinga1 connection parameters
$icingaurl="https://your.icinga.host.org";
$icingaport=443;   // change if using non-standard port
$icingacgi="cgi-bin/icinga/status.cgi";     // should not change

// Icinga2 connection parameters
$icinga2url="https://your.icinga2.host.org";
$icinga2port=5665;  // change if using non-standard port
$icinga2ca="/path/to/your/ca/cert.crt";
$icinga2apipath="v1/objects";               // should not change

// authentication for either version
$icingauser="username";
$icingapass="password";

// Icinga 1 queries are filters passed as url parameters
$queryhosts="style=hostdetail&hostprops=10&hoststatustypes=12&jsonoutput";
$querysvcs="serviceprops=10&servicestatustypes=28&jsonoutput";
$queryall="allproblems&hostprops=10&hoststatustypes=12&serviceprops=10&servicestatustypes=28&jsonoutput";
$queryhostgroups="hostgroup=all&style=summary&jsonoutput";
$queryhostgroup="style=overview&jsonoutput&hostgroup=";
$queryhost="style=detaili&jsonoutput&host=";
$queryhostgroupdetail="style=detail&jsonoutput&hostgroup=";
$queryservicedetail="jsonoutput&style=detail&servicestatustypes=28&hostgroup=";

// Icinga 2 queries are passed as an array to the RESTful web service
$query2hosts = array(
    'attrs' => array('name', 'state', 'last_check_result', 'display_name'),
    'filter' => 'host.state!=0&&host.downtime_depth==0&&host.acknowledgement==0'
);
$query2services = array(
    'attrs' => array('name', 'state', 'last_check', 'last_check_result', 'display_name', 'acknowledgement'),
    'joins' => array('host.name', 'host.state'),
    'filter' => 'service.state!=ServiceOK&&service.downtime_depth==0&&service.acknowledgement==0&&host.downtime_depth==0&&host.acknowledgement==0&&host.state==0'
);

$query2hostgroups = array(
    'attrs' => array('name','state'),
    'joins' => array('host.name','host.state','host.groups')
);

?>
